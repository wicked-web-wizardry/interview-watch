LocalStrategy = require('passport-local').Strategy
mongoose = require 'mongoose'
require './models/models'

Account = mongoose.model 'Account'

module.exports = (passport) ->
	passport.use new LocalStrategy
		usernameField: 'email'
		(email, password, done) ->
			Account.findOne email: email, (err, account) ->
				if err
					return done err
				if !account
					return done null, false, message: 'Incorrect email.'
				if !account.authenticate password
					return done null, false, message: 'Incorrect password.'
				return done null, account

	passport.serializeUser (user, done) ->
		done null, user._id

	passport.deserializeUser (id, done) ->
		Account.findById id, (err, user) ->
			done err, user