mongoose = require 'mongoose'
Schema = mongoose.Schema
crypto = require 'crypto'

###
AccountSchema
###

AccountSchema = new Schema
	email:
		type: String
		required: true
		unique: true
	hashedPassword: String
	salt: String
,
	collection: 'accounts'
	discriminatorKey: 'type'

###
Virtuals
###

AccountSchema.virtual 'password'
.set (password) ->
	@_password = password
	@salt = @makeSalt()
	@hashedPassword = @encryptPassword password

.get ->
	@_password

###
Validators
###

# validatePresenceOf = (value) -> value? and value.length > 0

AccountSchema.path 'email'
.validate (email) ->
	/[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*/.test email

, 'Invalid email.'

AccountSchema.path 'hashedPassword'
.validate (email) ->
	@password? and @password.length >= 6

, 'Password must have at least 6 characters.'

###
Pre-save hook
###

# AccountSchema.pre 'save', true, (next, done) ->
# 	return next() unless @isNew

# 	# Automatically save model and replace with its id
# 	if typeof @model is Company or
# 	typeof @model is Interviewee
# 		@model.save (err) ->
# 			return done err if err
# 			@model = @model._id
# 			done()
# 	else
# 		# Gotta call done after next()
# 		setTimeout done, 0

# 	next()

###
Methods
###

AccountSchema.methods =
	###
	Authenticate - check if the passwords are the same
	@param {String} plainText
	@return {Boolean}
	@api public
	###
	authenticate: (plainText) ->
		@encryptPassword(plainText) is @hashedPassword
	
	###
	Make salt
	@return {String}
	@api public
	###
	makeSalt: ->
		crypto.randomBytes(16).toString 'base64'
	
	###
	Encrypt password
	@param {String} password
	@return {String}
	@api public
	###
	encryptPassword: (password) ->
		return '' if not password or not @salt

		salt = new Buffer @salt, 'base64'
		crypto.pbkdf2Sync(password, salt, 10000, 64).toString 'base64'

mongoose.model 'Account', AccountSchema

module.exports = AccountSchema
