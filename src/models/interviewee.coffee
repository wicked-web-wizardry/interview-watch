mongoose = require 'mongoose'
Schema = mongoose.Schema

AccountSchema = require './account'

###
IntervieweeSchema
###

IntervieweeSchema = AccountSchema.extend
	name:
		type: String
		required: true
	address:
		type: String

mongoose.model 'Interviewee', IntervieweeSchema

module.exports = IntervieweeSchema