mongoose = require 'mongoose'
Schema = mongoose.Schema

AccountSchema = require './account'
ContactSchema = require './contact'
Contact = mongoose.model 'Contact'

###
CompanySchema
###

CompanySchema = AccountSchema.extend
	name:
		type: String
		required: true
	contacts: [ContactSchema]
	description: String
	primaryContact:
		type: Schema.ObjectId
		required: true
		ref: 'Contact'
###
Virtuals
###

# CompanySchema.virtual('email').get ->
# 	@contacts.reduce (contact) ->

###
Validators
###

CompanySchema.path 'contacts'
.validate (contacts) ->
	contacts.length > 0

, 'Cannot have zero contacts.'

CompanySchema.path 'primaryContact'
.validate (primaryContact) ->
	@contacts.some (contact) -> contact._id.equals(primaryContact)

, 'Primary contact must be in contacts list.'

###
Pre-save hook
###

# TODO: If no primary contact given, make it the first contact?

###
Methods
###

mongoose.model 'Company', CompanySchema

module.exports = CompanySchema
