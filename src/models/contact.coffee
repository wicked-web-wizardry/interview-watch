mongoose = require 'mongoose'
Schema = mongoose.Schema

ContactSchema = new Schema
	name:
		type: String
		required: true
	address: String
		# Easier to have user format?
		# address1: String
		# address2: String
		# city: String
		# province: String
		# postalCode: String
		# country: String
	email:
		type: String
	phone:
		type: String
	fax: String

empty = (str) -> not str? or str is ''

ContactSchema.path 'email'
.validate (email) ->
	empty(email) or
	/[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9-]+(\.[a-z0-9-]+)*/.test email

ContactSchema.pre 'validate', (next) ->
	if empty(@email) and empty(@phone)
		@invalidate 'email', 'Email required if phone number absent.', @email

	next()

mongoose.model 'Contact', ContactSchema

module.exports = ContactSchema