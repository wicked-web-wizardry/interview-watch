module.exports = {
	'AccountSchema': require './account'
	'IntervieweeSchema': require './interviewee'
	'CompanySchema': require './company'
	'ContactSchema': require './contact'
}