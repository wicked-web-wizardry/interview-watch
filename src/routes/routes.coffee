account = require '../controllers/account'
session = require '../controllers/session'

module.exports = (app) ->
	###
	Account resource
	###
	app.post '/api/account',
		account.createAccount
		account.loggedInAccount

	app.get '/api/account',
		session.ensureAuthenticated
		account.loggedInAccount

	app.put '/api/account',
		session.ensureAuthenticated
		account.updateUser
		account.loggedInAccount

	# app.del '/api/account', session.ensureAuthenticated, account.deleteAccount

	###
	Session resource
	###
	app.put '/api/session', session.login, session.session
	app.get '/api/session', session.ensureAuthenticated, session.session
	app.del '/api/session', session.logout