passport = require 'passport'
_ = require 'underscore'

exports.login = (req, res, next) ->
	passport.authenticate('local', (err, user, info) ->
		return next err if err?
		return res.json 401, error: info if not user?

		req.login user, next

	)(req, res, next)

exports.logout = (req, res, next) ->
	if req.user?
		req.logout()
		res.send 200, {}
	else
		res.send 400, 'Not logged in.'

exports.session = (req, res, next) ->
	res.json user: (_.pick req.user, '_id', 'name', 'email', 'type')

exports.ensureAuthenticated = (req, res, next) ->
	return next() if req.isAuthenticated()
	
	res.json 401, error: 'Not logged in.'