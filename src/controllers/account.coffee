mongoose = require 'mongoose'
Q = require 'q'
xssEscape = require 'xss-escape'
_ = require 'underscore'

config = require '../../config/config'

require '../models/models'
Account = mongoose.model 'Account'
Company = mongoose.model 'Company'
Interviewee = mongoose.model 'Interviewee'
Contact = mongoose.model 'Contact'

serializableFields = (type) ->
	switch type
		when 'Interviewee' then ['_id', 'email', 'name', 'type']
		when 'Company' then [
			'_id', 'email', 'name', 'type'
			'contacts', 'primaryContact'
		]

exports.createAccount = (req, res, next) ->
	req.body = xssEscape req.body # wreck my body

	(switch req.body.type
		when 'Interviewee'
			Q Interviewee.create
				name: req.body.name
				email: req.body.email
				password: req.body.password
				address: req.body.address

		when 'Company'
			primaryContact = new Contact req.body.contact

			Q Company.create
				name: req.body.name
				email: req.body.email
				password: req.body.password
				contacts: [primaryContact]
				primaryContact: primaryContact._id
	)
	.then (account) -> Q.all [account, (Q.ninvoke req, 'login', account)]

	.then ([account]) ->
		# Don't send this,
		#it double sends when you call account.loggedinaccount afterwards
		# res.json user: _.pick account, serializableFields account.type
		next()

	.fail (err) ->
		console.error err

		message = ''
		switch err.code
			# TODO: better error messages
			when 11000, 11001
				message = 'Email already registered,
					please use a different one.'
			else
				message = "Signup request failed to validate.
					Please contact #{config.contactEmail}."
		res.json 412,
			message: message
			err: err

		next err

exports.loggedInAccount = (req, res, next) ->
	res.json 200, _.pick req.user, serializableFields req.user.type

exports.updateUser = (req, res, next) ->
	if req.body._id isnt req.user._id
		return next new Error 'Given ID doesn\'t match logged in user\'s ID'

	_.extend req.user, _.pick req.body, serializableFields req.user.type

	req.user.save next

exports.deleteAccount = (req, res, next) ->
	next new Error 'This method is not implemented'