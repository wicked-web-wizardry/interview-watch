path = require 'path'
querystring = require 'querystring'
https = require 'https'

express = require 'express'
morgan = require 'morgan'
serveStatic = require 'serve-static'
favicon = require 'serve-favicon'
bodyParser = require 'body-parser'
cookieParser = require 'cookie-parser'
session = require 'express-session'

config = require '../config/config'

mongoose = require 'mongoose'
schemaExtend = require 'mongoose-schema-extend'

passport = require 'passport'
require('./passport-config')(passport)

db = mongoose.connect config.dbURL

# Express config
app = express()

app.use morgan()
app.use cookieParser()
app.use bodyParser()
app.use session
	secret: "The secret was you all along!"
	saveUninitialized: true
	resave: true
app.use passport.initialize()
app.use passport.session()

# app.use favicon path.join __dirname, '..', 'built-app/favicon.ico'

app.use '/bower_deps',
	serveStatic path.join __dirname, '..', 'bower_components'

app.use serveStatic path.join __dirname, '..', 'built-app'

# routes go here
require('./routes/routes')(app)

app.listen 80