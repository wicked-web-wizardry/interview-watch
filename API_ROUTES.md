# Resources
## Session
**URL**: `/api/session`

### **PUT** /api/session
Logs in to a specified account. `_id`, `email`, `name` and `type` is returned (for compatibility with **ngResource**).

### **GET** /api/session
Returns `_id`, `email`, `name` and `type` from the currently logged-in account (for compatibility with **ngResource**).

### **DEL** /api/session
Logs out from the currently logged-in account. An empty object is returned.

## Account

**URL**: `/api/account`
### **POST** /api/account
Registers a new account. Logs in to the newly-created account in the process, and returns it (for compatibility with **ngResource**).

### **GET** /api/account
Returns the user info for the currently logged-in account. For both companies and interviewees, this returns `_id`, `email`, `name` and `type`; for companies, `primaryContact`, and `contacts` are additionally returned.

### **PUT** /api/account
Updates user info for currently logged-in account. Returns the new user data (for compatibility with **ngResource**).

### **DELETE** /api/account (**UNIMPLEMENTED**)
Deletes the logged-in account, logging the user out in the process.