chai = require 'chai'
chai.use require 'chai-as-promised'

{expect} = chai
chai.should()

{asyncCatch} = require '../../common'

mongoose = require 'mongoose'
schemaExtend = require 'mongoose-schema-extend'

require '../../../src/models/models'

Interviewee = mongoose.model 'Interviewee'

describe 'Interviewee model', ->
	account = null

	it 'should allow saving an interviewee account', (done) ->
		account = new Interviewee
			name: 'Jebediah Rockefeller'
			email: 'jebediah.rockefeller@ica.se'
			password: 'marilyn'
			address: 'Luthagen, Uppsala'
		account.save done

	after (done) ->
		Interviewee.remove done