chai = require 'chai'
chai.use require 'chai-as-promised'

{expect} = chai
chai.should()

{asyncCatch} = require '../../common'

mongoose = require 'mongoose'
schemaExtend = require 'mongoose-schema-extend'

require '../../../src/models/models'

Contact = mongoose.model 'Contact'

describe 'Contact model', ->
	it 'should allow specifying e-mail', (done) ->
		contact = new Contact
			name: 'Norway HQ'
			address: 'Senya, Norway'
			email: 'email.norway@rockpolishinc.com'

		contact.validate asyncCatch(done) (err) ->
			expect(err).to.not.exist

			done()

	it 'should allow having a phone number without e-mail', (done) ->
		contact = new Contact
			name: 'Norway HQ'
			address: 'Senya, Norway'
			phone: '46 998 10 3050'

		contact.validate asyncCatch(done) (err) ->
			expect(err).to.not.exist

			done()

	it 'should be rejected if it has neither a phone number
	    or e-mail', (done) ->

		contact = new Contact
			name: 'Norway HQ'
			address: 'Senya, Norway'

		contact.validate asyncCatch(done) (err) ->
			expect(err).to.exist

			expect(err.errors.email).to.exist

			err.errors.email.message
			.should.match /email required if phone number absent/i

			done()