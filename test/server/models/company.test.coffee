chai = require 'chai'
chai.use require 'chai-as-promised'

{expect} = chai
chai.should()

{asyncCatch} = require '../../common'

mongoose = require 'mongoose'
schemaExtend = require 'mongoose-schema-extend'

require '../../../src/models/models'

Company = mongoose.model 'Company'

describe 'Company model', ->
	account = null

	it 'should allow saving a company account', (done) ->
		account = new Company
			name: 'Rock Polish Inc.'
			email: 'email@rockpolishinc.com'
			password: 'hurr123'
			description: 'Rock on, polishers!'
			contacts: [
				{
					name: 'Norway HQ'
					address: 'Senya, Norway'
					email: 'email.norway@rockpolishinc.com'
				}
			]
		account.primaryContact = account.contacts[0]._id
		account.save done

	after (done) ->
		Company.remove done