{expect} = chai
chai.should()

describe 'Sign up component', ->
	beforeEach ->
		module 'ui.router'
		module 'iwSignup'

	describe 'Common signup controller', ->
		scope = null
		ctrl = null

		beforeEach inject ($rootScope, $controller) ->
			scope = $rootScope.$new()

			ctrl = $controller 'SignupCtrl', $scope: scope

		it 'should default to showing interviewee signup', ->
			scope.currentVisibleSignup.should.equal 'interviewee'

			scope.isIntervieweeVisible().should.be.true
			scope.isCompanyVisible().should.be.false

		it 'should allow setting company as current shown signup page', ->
			scope.showCompany()

			scope.currentVisibleSignup.should.equal 'company'

			scope.isIntervieweeVisible().should.be.false
			scope.isCompanyVisible().should.be.true

		it 'should allow resetting back to interviewee page', ->
			scope.showCompany()
			scope.showInterviewee()

			scope.currentVisibleSignup.should.equal 'interviewee'

			scope.isIntervieweeVisible().should.be.true
			scope.isCompanyVisible().should.be.false

	describe 'Company signup controller', ->
		$httpBackend = null
		scope = null
		ctrl = null

		beforeEach inject ($rootScope, $controller, _$httpBackend_) ->
			$httpBackend = _$httpBackend_

			$httpBackend.when 'POST', '/api/company/create'
			.respond (method, url, data, headers) ->
				data = JSON.parse data
				[
					200
					JSON.stringify { email: data.email }
					{ 'Content-Type': 'application/json' }
					''
				]

			scope = $rootScope.$new()
			ctrl = $controller 'CompanySignupCtrl', $scope: scope

		# it 'should POST to /api/company/create', ->
		# 	$httpBackend.expect 'POST', '/api/company/create'

		# 	scope.formModel =
		# 		name: 'Boat C.o'
		# 		email: 'boats@boat.com'
		# 		password: 'boats'
		# 		confirmPassword: 'boats'
		# 		contact:
		# 			name: 'Denmark Division'
		# 			address: 'King Road To The Left'
		# 			email: 'recruiter@boat.com'

		# 	scope.submit()

		# 	$httpBackend.flush()

	describe 'Interviewee signup controller', ->
		$httpBackend = null
		scope = null
		ctrl = null

		beforeEach inject ($rootScope, $controller, _$httpBackend_) ->
			$httpBackend = _$httpBackend_

			# insert responses here

			scope = $rootScope.$new()
			ctrl = $controller 'CompanySignupCtrl', $scope: scope