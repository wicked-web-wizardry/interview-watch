angular.module 'interviewWatchApp', [
	'ui.bootstrap', 'ui.router'

	'iwAuth'
	'iwHome'
	'iwCompanyDashboard'
	'iwIntervieweeDashboard'
	'iwJobPosting'
	'iwApplicantView'
	'iwInterview'
	'iwLogin'
	'iwSignup'
]

# .config ($locationProvider) ->
# 	$locationProvider.html5Mode true

.config ($stateProvider, $urlRouterProvider) ->
	$urlRouterProvider.otherwise '/'

	$stateProvider
		.state 'home',
			url: '/'
			templateUrl: 'home/home.html'
			controller: 'HomeCtrl'

		.state 'login',
			url: '/login'
			templateUrl: 'login/login.html'
			controller: "LoginCtrl"

		.state 'signup',
			url: '/signup'
			templateUrl: 'signup/signup.html'
			controller: 'SignupCtrl'

		.state 'dashboard',
			url: '/dashboard'
			templateProvider: ($rootScope, $http, $templateCache) ->
				url = switch $rootScope.user?.type
					when 'Company'
						'company-dashboard/company-dashboard.html'
					when 'Interviewee'
						'interviewee-dashboard/interviewee-dashboard.html'
				return $http
					.get url, cache: $templateCache
					.then (res) -> return res.data

			controllerProvider: ($rootScope) ->
				switch $rootScope.user?.type
					when 'Company'
						'CompanyDashboardCtrl'
					when 'Interviewee'
						'IntervieweeDashboardCtrl'

		.state 'jobPosting',
			url: '/company/:companyId/job-posting/:jobPostingId'
			templateUrl: 'job-posting/job-posting.html'
			controller: 'JobPostingCtrl'

		.state 'interview',
			url: '/company/:companyId/job-posting/:jobPostingId/interview'
			templateUrl: 'interview/interview.html'
			controller: 'InterviewCtrl'

		.state 'applicantView',
			url: '/company/:companyId/job-posting/:jobPostingId/applicant/:applicantId'
			templateUrl: 'applicant-view/applicant-view.html'
			controller: 'ApplicantViewCtrl'

.run ($rootScope, $state, editableOptions) ->
	authStates = ['intervieweeDashboard', 'companyDashboard', 'dashboard']

	$rootScope.$on '$stateChangeStart',
		(event, toState, toParams, fromState, fromParams) ->
			if toState.name in authStates and not $rootScope.isLoggedIn()
				event.preventDefault()

				$state.go 'login'

	editableOptions.theme = 'bs3'
	
.controller 'RootCtrl', ($rootScope, authService) ->
	$rootScope.user = null

	# $rootScope.signupCompany = (model, cb) ->
	# 	authService.signupCompany model, (err, user) ->
	# 		$rootScope.user = user if not err
	# 		cb err, user

	# $rootScope.signupInterviewee = (model, cb) ->
	# 	authService.signupInterviewee model, (err, user) ->
	# 		$rootScope.user = user if not err
	# 		cb err, user

	# $rootScope.login = (email, password, cb) ->
	# 	authService.login email, password, (err, user) ->
	# 		$rootScope.user = user if not err
	# 		cb err, user

	# $rootScope.logout = (cb) ->
	# 	authService.logout (args...) ->
	# 		$rootScope.user = null
	# 		cb(args...)

	$rootScope.isLoggedIn = -> $rootScope.user?