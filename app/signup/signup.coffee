angular.module 'iwSignup', ['ui.validate']

.controller 'SignupCtrl', ($scope, $stateParams, $http) ->
	$scope.currentVisibleSignup = 'interviewee'

	$scope.showInterviewee = ->
		$scope.currentVisibleSignup = 'interviewee'

	$scope.isIntervieweeVisible = ->
		$scope.currentVisibleSignup is 'interviewee'

	$scope.showCompany = ->
		$scope.currentVisibleSignup = 'company'

	$scope.isCompanyVisible = ->
		$scope.currentVisibleSignup is 'company'

.controller 'CompanySignupCtrl',
($scope, $stateParams, $state, $http, authService) ->
	$scope.formModel = {}
	$scope.submitError = null

	$scope.empty = (input) ->
		not input? or input is ''

	$scope.submit = ->
		authService.signupCompany $scope.formModel
		.then (company) ->
			$state.go 'dashboard'
		.catch (err) ->
			$scope.submitError = err.message

.controller 'IntervieweeSignupCtrl',
($scope, $stateParams, $state, $http, authService) ->
	$scope.formModel = {}
	$scope.submitError = null

	$scope.submit = ->
		authService.signupInterviewee $scope.formModel
		.then (interviewee) ->
			$state.go 'dashboard'
		.catch (err) ->
			$scope.submitError = err.message