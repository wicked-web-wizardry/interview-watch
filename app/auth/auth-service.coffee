angular.module 'iwAuth', ['ngResource']

.factory 'Session', ($resource) ->
	return $resource '/api/session', null,
		login:
			method: 'PUT'
		logout:
			method: 'DELETE'

.factory 'Account', ($resource) ->
	return $resource '/api/account', null,
		update:
			method: 'PUT'

.factory 'authService', ($http, $rootScope, $state, Session, Account) ->

	###
	Sends a request to create a new Company.
	Model should have this structure:
		name:
		email:
		password:
		contact:
			name:
			address:
			email:
			phone:
			fax:

	Returns the object that will be populated
	with the new company later. You can access
	its $promise property for callbacks.
	###
	signupCompany: (model) ->
		company = new Account model
		company.type = "Company"
		return company.$save().then (company) ->
			$rootScope.user = company

	###
	Sends a request to create a new Interviewee.
	Model should have a name, email, and password.

	Returns the object that will be populated
	with the new company later. You can access
	its $promise property for callbacks.
	###
	signupInterviewee: (model) ->
		interviewee = new Account model
		interviewee.type = "Interviewee"
		return interviewee.$save().then (interviewee) ->
			$rootScope.user = interviewee

	login: (email, password) ->
		session = new Session {email, password}
		return session.$login().then (interviewee) ->
			$rootScope.user = session


	logout: () ->
		return Session.$delete.then ->
			$rootScope.user = null
			$state.go 'home'
