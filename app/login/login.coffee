angular.module 'iwLogin', []

.controller 'LoginCtrl', ($scope, $stateParams, $state, $http, authService) ->
	$scope.submit = ->
		authService.login $scope.email, $scope.password
		.then (account) ->
			$state.go 'dashboard'
		.catch (err) ->
			console.error err